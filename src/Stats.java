import exceptions.CounterExceededException;

public class Stats {
	
	public static final int MAX_ARCHERS = 15;
	public static final int MAX_WARRIORS = 20;
	public static final int MAX_TROOPS = 25;
	public static final int MAX_TOWERS = 10;
	public static final int MAX_BASE_COUNT = 1;
	public static final int MAX_WALL_COUNT = 50;
	
	public static final int INITIAL_BASE_HP = 1000;
	public static final int INITIAL_WARRIOR_HP = 100;
	public static final int INITIAL_ARCHER_HP = 75;
	public static final int INITIAL_TOWER_HP = 300;
	
	public static final int WARRIOR_DAMAGE = 25;
	public static final int ARCHER_DAMAGE = 30;
	public static final int TOWER_DAMAGE = 40;
	
	private int warriors;
	private int archers;
	private int baseHP;
	private int towerCount;
	private int baseCount;
	private int wallCount;
	
	public Stats(int baseHP, int warriors, int archers, int towerCount, int baseCount, int wallCount){
		this.warriors = warriors;
		this.archers = archers;
		this.towerCount = towerCount;
		this.baseHP = baseHP;
		this.setBaseCount(baseCount);
		this.wallCount = wallCount;
	}
	
	public Stats(){
		this(INITIAL_BASE_HP,0,0,0,0,0);	
	}

	public int getWarriors() {
		return warriors;
	}
	
	public int getWallCount(){
		return wallCount;
	}

	public void addWarriors(int warriors) {
		if(this.warriors+this.archers == MAX_TROOPS && warriors > 0){
			throw new CounterExceededException("You can only create up to "+MAX_TROOPS+" troops!");
		}
		this.warriors += warriors;
	}
	
	public void addArchers(int archerCount){
		if(this.warriors+this.archers == MAX_TROOPS && archerCount > 0){
			throw new CounterExceededException("You can only create up to "+MAX_TROOPS+" troops!");
		}
		this.archers+=archerCount;
	}
	
	public int getArchers() {
		return archers;
	}
	
	public void addWall(int wallCount){
		if(this.wallCount == MAX_WALL_COUNT && wallCount > 0){
			throw new CounterExceededException("You can only create up to "+MAX_WALL_COUNT+"walls!");
		}
		this.wallCount += wallCount;
	}
	
	

	public int getBaseHP() {
		return baseHP;
	}

	public int getTowerCount() {
		return towerCount;
	}
	
	public void addTower(int tower){
		if(this.towerCount == MAX_TOWERS && tower > 0){
			throw new CounterExceededException("You can only create up to "+MAX_TOWERS+" towers!");
		}
		towerCount+=tower;
	}

	public int getBaseCount() {
		return baseCount;
	}

	public void setBaseCount(int baseCount) {
		if(this.baseCount == 1 && baseCount > 0){
			throw new CounterExceededException("Your castle is already set!");
		}
		this.baseCount = baseCount;
	}
	
	public void setBaseHP(int baseHP){
		this.baseHP = baseHP;
	}
	
	public void reset(){
		warriors = archers = 0;
	}
	
	@Override
	public String toString(){
		StringBuilder builder = new StringBuilder();
		builder.append("WARRIORS:").append(warriors).append(";")
		.append("ARCHERS:").append(archers).append(";")
		.append("BASE:").append(baseCount).append(";");
		
		return builder.toString();
	}
}
