import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.text.DefaultCaret;


public class ChatPanel extends JPanel {

	private static final long serialVersionUID = -2613494144670085375L;
	 final JButton send;
	private final JTextField input;
	private final JTextArea chatLogs;
	
	public ChatPanel(){
		super();
		setLayout(new BorderLayout());
		setBackground(Color.GRAY);
		setPreferredSize(new Dimension(140,240));
		
		chatLogs = new JTextArea(12,10);
		send = new JButton("SEND");
		input = new JTextField(10);
		
		chatLogs.setEditable(false);
		chatLogs.setLineWrap(true);
		
		setupChatStuff();
		
	}

	private void setupChatStuff() {
		/*chat logs*/
		DefaultCaret caret = (DefaultCaret)chatLogs.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		add(new JScrollPane(chatLogs), BorderLayout.NORTH);
		
		/*input and button*/
		add(input, BorderLayout.CENTER);
		add(send, BorderLayout.SOUTH);
		
	}

	public void setupSendButton(final ActionListener actionListener) {
		send.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				input.setText("");
			}
		});
		send.addActionListener(actionListener);
	}

	public String getChatMessage() {
		return input.getText();
	}

	public void clearChatMessage() {
		input.setText("");
	}
	
	public void appendChatMessage(String username, String message){
		chatLogs.append("["+username+"] "+message+"\n");
	}
}
