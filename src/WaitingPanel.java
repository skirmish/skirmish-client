import java.awt.Color;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JPanel;


public class WaitingPanel extends JPanel{
	private static final long serialVersionUID = -7635706721597737333L;
	private final JPanel main;
	public WaitingPanel(){
		main = Utils.createEmptyPanel(450, 150, Color.DARK_GRAY);
		setPreferredSize(new Dimension(450,450));
		setBackground(Color.LIGHT_GRAY);
		add(Utils.createEmptyPanel(450, 150, Color.LIGHT_GRAY));
		add(main);
	}
	
	public void addPlayer(String username){
		JPanel bago = new JPanel();
		bago.setBorder(BorderFactory.createTitledBorder(username));
		bago.setPreferredSize(new Dimension(140,140));
		main.add(bago);
		revalidate();
	}
}
