import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.text.DefaultCaret;


public class LoggerPanel extends JPanel {
	private static final long serialVersionUID = 1016842340895232049L;
	private final JTextArea logger;
	
	public LoggerPanel(String title){
		super();
		setLayout(new BorderLayout());
		setBackground(Color.GRAY);
		setBorder(BorderFactory.createTitledBorder(title));
		logger = new JTextArea();
		
		logger.setEditable(false);
		logger.setLineWrap(true);
		
		setupTextArea();
		
	}
	
	public void loggerBackground(Color color){
		logger.setBackground(color);
	}
	
	public void loggerForeground(Color color){
		logger.setForeground(color);
	}
	
	private void setupTextArea() {
		/*chat logs*/
		DefaultCaret caret = (DefaultCaret)logger.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		add(new JScrollPane(logger), BorderLayout.NORTH);
	}
	
	public void clear(){
		logger.setText("");
	}
	
	public void log(String message){
		logger.append(message+"\n");
	}
}
