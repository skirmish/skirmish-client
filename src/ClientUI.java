import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;



public class ClientUI{
	private final JFrame frame;
	private final ChatPanel chatPanel;
	private final HomePanel homePanel;
	private final StatsPanel statsPanel;
	private String username;
	
	public ClientUI(String username){
		this.username = username;
		frame = new JFrame("SKIRMISH | "+username);
		chatPanel = new ChatPanel();
		homePanel = new HomePanel();
		statsPanel = new StatsPanel();
		init();
	}
	
	private void init(){
		setupCanvas();
		
		frame.pack();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
	}
	
	public void display(){
		frame.setVisible(true);
	}
	
	private JPanel explorerPanel;
	private JButton editBase;
	private JButton shop;
	private JButton attack;
	private JPanel east;
	private JButton save;
	private JButton warrior;
	private JButton archer;
	
	private void setupCanvas() {
		
		/*ACTION LISTENERS*/
		ActionListener forEditBase = new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				JButton source = (JButton)e.getSource();
				String structurestr = source.getText();
				Structure structure = Structure.FLOOR;
				switch(structurestr){
					case "CASTLE": structure = Structure.CASTLE; break;
					case "WALL": structure = Structure.WALL; break;
					case "TOWER": structure = Structure.TOWER; break;
					case "FLOOR": break;
				}
				homePanel.getBaseOf(username).setActiveStructure(structure);
			}
		};
		
		ActionListener forMainActions = new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				JButton source = (JButton)e.getSource();
				String action = source.getText();
				CardLayout cl = (CardLayout)east.getLayout();
				cl.show(east, action);
			}
		};
		
		JPanel canvas = new JPanel();
		canvas.setLayout(new BorderLayout());
		
		JPanel west = new JPanel(new BorderLayout());
		west.add(statsPanel,BorderLayout.NORTH);
		west.add(chatPanel,BorderLayout.SOUTH);
		
		canvas.add(west, BorderLayout.WEST);
		
		JPanel center = new JPanel(new BorderLayout());
		center.add(homePanel, BorderLayout.CENTER);
		
		explorerPanel = new JPanel();
		
		center.add(explorerPanel, BorderLayout.SOUTH);
		
		east = new JPanel(new CardLayout());
		
		JPanel controller = new JPanel(new GridLayout(3,1));
		
		editBase = new JButton("EDIT");
		
		controller.add(editBase);
		
		JPanel editBaseButtons = new JPanel(new GridLayout(6,1));
		JButton putBase = new JButton("CASTLE");
		putBase.addActionListener(forEditBase);
		JButton putWall = new JButton("WALL");
		putWall.addActionListener(forEditBase);
		JButton putTower = new JButton("TOWER");
		putTower.addActionListener(forEditBase);
		JButton erase = new JButton("ERASE");
		erase.addActionListener(forEditBase);
		
		save = new JButton("SAVE");
		
		JButton back = new JButton("BACK");
		editBaseButtons.add(putBase);
		editBaseButtons.add(putTower);
		editBaseButtons.add(putWall);
		editBaseButtons.add(erase);
		editBaseButtons.add(save);
		editBaseButtons.add(back);
		
		editBase.addActionListener(forMainActions);
		
		back.addActionListener(forMainActions);

		shop = new JButton("ARMY");
		controller.add(shop);
		
		JPanel shopping =  new JPanel(new GridLayout(4,1));
		
		warrior = new JButton("WARRIOR");
		archer = new JButton("ARCHER");
		JButton back2 = new JButton("BACK");
		
		shopping.add(warrior);
		shopping.add(archer);
		shopping.add(back2);
		
		back2.addActionListener(forMainActions);
		shop.addActionListener(forMainActions);
		
		attack = new JButton("ATK");
		controller.add(attack);
		
		east.add("BACK", controller);
		east.add("EDIT",editBaseButtons);
		east.add("ARMY", shopping);
		center.add(east, BorderLayout.EAST);
		
		canvas.add(center, BorderLayout.CENTER);
		
		frame.setContentPane(canvas);
	}
	
	public void setupAttackButton(ActionListener l){
		attack.addActionListener(l);
	}

	public void setupSendButton(final ActionListener actionListener) {
		chatPanel.setupSendButton(actionListener);
	}
	
	public void setupSaveBaseButton(ActionListener actionListener){
		save.addActionListener(actionListener);
	}
	
	public String getChatMessage() {
		return chatPanel.getChatMessage();
	}
	
	public void appendChatMessage(String username, String message){
		chatPanel.appendChatMessage(username, message);
	}

	public void clearChatMessage() {
		chatPanel.clearChatMessage();
		
	}

	public void addPlayerToWaitingArea(String username_new) {
		homePanel.addNewPlayer(username_new);
	}

	public void show(String username) {
		homePanel.show(username);
		if(username.equals(this.username)){
			editBase.setEnabled(true);
			shop.setEnabled(true);
		}else{
			editBase.setEnabled(false);
			shop.setEnabled(false);
			CardLayout cl = (CardLayout)east.getLayout();
			cl.show(east, "CONTROLLER");
		}
	}

	public void createBaseFor(String username_new) {
		JButton selector = new JButton(username_new);
		selector.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				JButton source = (JButton)e.getSource();
				show(source.getText());
			}
		});
		selector.setEnabled(false);
		explorerPanel.add(selector);
		homePanel.addPlayerBase(username_new);
		
	}

	public String getHomeBaseState() {
		BasePanel ownBase = homePanel.getBaseOf(this.username);
		return ownBase.getState();
	}

	public void updateBaseState(String username, String baseState) {
		BasePanel base = homePanel.getBaseOf(username);
		base.updateState(baseState);
	}

	public void enableJumpButtons(boolean enabled) {
		for(Component c : explorerPanel.getComponents()){
			((JButton)c).setEnabled(enabled);
		}
	}

	public void enableActionButtons(boolean enabled) {
		editBase.setEnabled(enabled);
		shop.setEnabled(enabled);
		attack.setEnabled(enabled);
	}

	public void refreshStats(Stats stats) {
		statsPanel.show(stats);
	}
	
	public void setupBuyButtons(ActionListener l){
		warrior.addActionListener(l);
		archer.addActionListener(l);
	}

	public String getActivePanelUsername() {
		return homePanel.getActiveBaseUsername();
	}
	
	public void setStats(Stats stats, String username){
		homePanel.setStatsOf(username,stats);
	}

	public void disableEditBase() {
		editBase.setEnabled(false);
	}

	public void enableControls(boolean b) {
		enableActionButtons(b);
		enableJumpButtons(b);
	}
}