
public class Unit {
	private int HP;
	private int damage;
	
	private Unit(){};
	private Unit(int HP, int damage){
		this.HP = HP;
		this.damage = damage;
	}
	
	public static final int INITIAL_BASE_HP = 1000;
	public static final int INITIAL_WARRIOR_HP = 100;
	public static final int INITIAL_ARCHER_HP = 75;
	public static final int INITIAL_TOWER_HP = 300;
	
	public static final int WARRIOR_DAMAGE = 25;
	public static final int ARCHER_DAMAGE = 30;
	public static final int TOWER_DAMAGE = 40;
	
	public static Unit createWarrior(){
		return new Unit(INITIAL_WARRIOR_HP, WARRIOR_DAMAGE);
	}
	
	public static Unit createArcher(){
		return new Unit(INITIAL_ARCHER_HP, ARCHER_DAMAGE);
	}
	
	public static Unit createTower(){
		return new Unit(INITIAL_TOWER_HP, TOWER_DAMAGE);
	}
	
	public void attack(Unit opponent){
		opponent.HP -= this.HP;
	}
	
	public int getHP(){ return HP; }
	public int getDamage(){ return damage; }
}
