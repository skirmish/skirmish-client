import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Arrays;

import javax.swing.JPanel;

import exceptions.CounterExceededException;

public class BasePanel extends JPanel{
	private static final long serialVersionUID = 3409718392077034201L;
	private static final char WALL = 'W';
	private static final char CASTLE = 'C';
	private static final char TOWER = 'T';
	private static final char FLOOR = 'F';
	private static final char RUINS = 'R'; //used for deleting castle
	private static final int GRID_SIZE = 15;
	private static final int PIXEL_GRID = 450/GRID_SIZE;
	
	private Structure activeStructure;
	private char[] state;
	private String username;
	private Stats stats; 
	public BasePanel(String owner){
		setUsername(owner);
		setPreferredSize(new Dimension(450,450));
		activeStructure = null;
		initializeState();
		setupMouseListener();
	}
	
	public void setStats(Stats stats){
		this.stats = stats;
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	private void setupMouseListener() {
		addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {}
			
			@Override
			public void mousePressed(MouseEvent e) {
				
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				int x = e.getX();
				int y = e.getY();
				x = x/PIXEL_GRID;
				y = y/PIXEL_GRID;
				char previous = getStructureOnCell(x, y);
				setStructureOnCell(x, y, activeStructure);
				char current = getStructureOnCell(x, y);
				if(previous!=current)updateCell(x, y);
				System.out.println(stats);
			}
		});
		
	}

	private void initializeState(){
		state = new char[GRID_SIZE*GRID_SIZE];
		Arrays.fill(state, FLOOR);
	}
	
	public Structure getActiveStructure() {
		return activeStructure;
	}

	public void setActiveStructure(Structure activeStructure) {
		this.activeStructure = activeStructure;
	}
	
	private char getStructureOnCell(int x, int y){
		if(x>=GRID_SIZE || y >=GRID_SIZE)return FLOOR;
		return state[x*GRID_SIZE+y];
	}
	
	private void setStructureOnCell(int x, int y, Structure structure){
		
		if(structure == null){
			return;
		}
		switch(structure){
			case FLOOR: clear(x,y); break;
			case WALL: setupWall(x,y); break;
			case TOWER: setupTower(x,y); break;
			case CASTLE: setupCastle(x, y); break;
		}
	}
	
	private void setupTower(int x, int y){
		if(getStructureOnCell(x, y) != FLOOR)return;
		state[x*GRID_SIZE+y] = TOWER;
	}
	
	private void setupWall(int x, int y){
		if(getStructureOnCell(x, y) != FLOOR)return;
		state[x*GRID_SIZE+y] = WALL;
	}
	
	private void clear(int x, int y){
		if(getStructureOnCell(x, y) == CASTLE){
			state = String.valueOf(state).replaceAll(""+CASTLE, ""+RUINS).toCharArray();
			stats.setBaseCount(0);
		}else{
			switch(getStructureOnCell(x, y)){
			case TOWER: stats.addTower(-1); break;
			case WALL: stats.addWall(-1); break;
			}
			state[x*GRID_SIZE+y] = FLOOR;
		}
		
	}

	private void setupCastle(int x, int y) {
		char s = CASTLE;
		char UL, U, UR, Le, R, LL, Lo, LR;
		if(x==0 || x==GRID_SIZE-1 || y==0 || y==GRID_SIZE-1){
			return;
		}
		if(getStructureOnCell(x, y) != FLOOR)return;
		UL = getStructureOnCell(x-1, y-1);
		if(UL!=FLOOR)return;
		U = getStructureOnCell(x, y-1);
		if(U!=FLOOR)return;
		UR = getStructureOnCell(x+1, y-1);
		if(UR!=FLOOR)return;
		Le = getStructureOnCell(x-1, y);
		if(Le!=FLOOR)return;
		R = getStructureOnCell(x+1, y);
		if(R!=FLOOR)return;
		LL = getStructureOnCell(x-1, y+1);
		if(LL!=FLOOR)return;
		Lo = getStructureOnCell(x, y+1);
		if(Lo!=FLOOR)return;
		LR = getStructureOnCell(x+1, y+1);
		if(LR!=FLOOR)return;
		if(x-1>=0){
			state[(x-1)*GRID_SIZE+(y)] = s;
			if(y-1>=0)state[(x-1)*GRID_SIZE+(y-1)] = s; 
			if(y+1<GRID_SIZE)state[(x-1)*GRID_SIZE+(y+1)] = s;
		}
		
		if(x+1<GRID_SIZE){
			state[(x+1)*GRID_SIZE+(y)] = s;
			if(y-1>=0)state[(x+1)*GRID_SIZE+(y-1)] = s;
			if(y+1<GRID_SIZE)state[(x+1)*GRID_SIZE+(y+1)] = s;
		}
		
		if(y-1>=0)state[(x)*GRID_SIZE+(y-1)] = s;
		if(y+1<GRID_SIZE)state[(x)*GRID_SIZE+(y+1)] = s;
		state[x*GRID_SIZE+y] = s;
	}
	
	private void updateCell(int x,int y){
		Color color = Color.WHITE;
		try{
		switch(state[x*GRID_SIZE+y]){
		 case FLOOR: 
			color = Color.WHITE;
			colorCell(x,y,color);
			drawBorder(x, y);
		 	break;
		 case WALL: 
			 stats.addWall(1);
			 color = Color.LIGHT_GRAY; 
			 colorCell(x,y,color);
			 drawBorder(x, y);
			 
			 break;
		 case CASTLE: 
			stats.setBaseCount(1);
			color = Color.YELLOW; 
			colorCellCenter(x,y,2,color);
			drawBorder(x,y,2);
			
			break;
		 case TOWER: 
			 stats.addTower(1);
			color = Color.BLUE; 
		 	colorCell(x,y,color);
			drawBorder(x, y);
			
			break;
		 case RUINS:
			 color = Color.WHITE;
			 int cunt=0;
			 for(int i=x-3; i<GRID_SIZE && i<x+3;i++){
				 if(cunt==9)break;
				 for(int j=y-3; j<GRID_SIZE && j<y+3;j++){
					 if(state[i*GRID_SIZE+j] == RUINS){
						 colorCell(i, j, color);
						 drawBorder(i,j);
						 cunt++;
						 state[i*GRID_SIZE+j] = FLOOR;
					 }
				 }
			 }
			 break;
		}}catch(CounterExceededException cee){
			Utils.popupError(cee.getMessage());
		}
		revalidate();
	}
	
	private void colorCell(int x, int y, Color color){
		Graphics g = getGraphics();
		g.setColor(color);
		g.fillRect(x*PIXEL_GRID, y*PIXEL_GRID, PIXEL_GRID, PIXEL_GRID);
	}
	
	private void colorCellCenter(int xCenter, int yCenter, int size, Color color){
		for(int i=xCenter-1; i>=0 && i<GRID_SIZE && i<xCenter+size; i++){
			for(int j=yCenter-1; j>=0 && j<GRID_SIZE && j<yCenter+size; j++){
				colorCell(i,j,color);
			}
		}
	}
	
	
	private void drawBorder(int x,int y){
		Graphics g = getGraphics();
		g.setColor(Color.BLACK);
		g.drawRect(x*PIXEL_GRID, y*PIXEL_GRID, PIXEL_GRID, PIXEL_GRID);
	}
	
	private void drawBorder(int x, int y, int size){
		Graphics g = getGraphics();
		g.setColor(Color.BLACK);
		g.drawRect((x-1)*PIXEL_GRID, (y-1)*PIXEL_GRID, PIXEL_GRID*(size+1), PIXEL_GRID*(size+1));
	}
	
	public String getState(){
		return String.valueOf(state);
	}

	@Override
	public void paintComponent(Graphics g){
		boolean drawn = false;
		for(int i=0; i<GRID_SIZE; i++){
			for(int j=0; j<GRID_SIZE; j++){
				Color color = Color.WHITE;
				if(state!=null){
					switch(state[i*GRID_SIZE+j]){
						case FLOOR: color = Color.WHITE; break;
						case WALL: color = Color.LIGHT_GRAY; break; 
						case TOWER: color = Color.BLUE; break;
						case CASTLE: 
							color = Color.YELLOW; 
							if(!drawn){
								for(int aa=i; aa<i+3; aa++){
									for(int bb=j; bb<j+3; bb++ ){
										g.setColor(color);
										g.fillRect(aa*PIXEL_GRID, bb*PIXEL_GRID, PIXEL_GRID, PIXEL_GRID);
									}
								}
								g.setColor(Color.black);
								g.drawRect(PIXEL_GRID*i,PIXEL_GRID*j,PIXEL_GRID*3,PIXEL_GRID*3);
								drawn = true;
							}
							continue;
					}
				}
				
				g.setColor(color);
				g.fillRect(i*PIXEL_GRID, j*PIXEL_GRID, PIXEL_GRID, PIXEL_GRID);
				g.setColor(Color.black);
				g.drawRect(PIXEL_GRID*i,PIXEL_GRID*j,PIXEL_GRID,PIXEL_GRID);
			}
		}
	}

	public void updateState(String baseState) {
		this.state = baseState.toCharArray();
		this.repaint();
		
	}
}
