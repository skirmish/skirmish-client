import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JPanel;


public class HomePanel extends JPanel {

	private static final long serialVersionUID = -8742357999390766582L;
	private final WaitingPanel waitingPanel;
	private Map<String, BasePanel> bases;
	private String activeBase;

	public HomePanel(){
		waitingPanel = new WaitingPanel();
		bases = new HashMap<String, BasePanel>();
		setLayout(new CardLayout());
		setBackground(Color.LIGHT_GRAY);
		setPreferredSize(new Dimension(450,450));
		setupOthers();
		show("WAITING_AREA");
	}

	private void setupOthers() {
		add("WAITING_AREA",waitingPanel);
	}
	
	public void addPlayerBase(String username){
		bases.put(username, new BasePanel(username));
		add(username,bases.get(username));
	}
	
	public void show(String key){
		((CardLayout)getLayout()).show(this, key);
		activeBase = key;
	}
	
	public void addNewPlayer(String username){
		waitingPanel.addPlayer(username);
		revalidate();
	}
	
	public BasePanel getBaseOf(String username){
		return bases.get(username);
	}

	public String getActiveBaseUsername() {
		return activeBase;
		
	}

	public void setStatsOf(String username, Stats stats) {
		bases.get(username).setStats(stats);
	}
}
