import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import exceptions.InvalidPortException;
import exceptions.InvalidServerAddressException;
import exceptions.InvalidUsernameException;


public class Main {

	public static void main(String[] args) {
		String username = Utils.askUsername();
		if(username == null)System.exit(0);
		
		String address = Utils.askAddress();
		if(address == null)System.exit(0);
		
		int port = Utils.askPort();
		if(port == 0)System.exit(0);
		
		Client cl = new Client(username, address, port);
		//new ClientUI("clinton").display();
		//testPanel(new StatsPanel());
	}
	
	public static void testPanel(JPanel panel){
		JFrame frame = new JFrame("TEST");
		frame.setContentPane(panel);
		frame.pack();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
	
	public static String askUsername(){
		String username = "";
		while(username == ""){
			username = JOptionPane.showInputDialog(null, "Username: ", "SKIRMISH | JOIN", JOptionPane.QUESTION_MESSAGE);
			if(username == null){
				return null;
			}else{
				try{
					Validate.checkUsername(username);
				}catch(InvalidUsernameException iue){
					Utils.popupError(iue.getMessage());
					username = "";
				}
			}
		}
		return username;
	}
	
	public static String askAddress(){
		String address = "";
		while(address == ""){
			address = JOptionPane.showInputDialog(null, "Server address: ", "SKIRMISH | JOIN", JOptionPane.QUESTION_MESSAGE);
			if(address == null){
				return null;
			}else{
				try{
					Validate.checkAddress(address);
				}catch(InvalidServerAddressException iae){
					Utils.popupError(iae.getMessage());
					address = "";
				}
			}
		}
		return address;
	}
	
	public static int askPort(){
		int port = 0;
		while(port == 0){
			String portStr = JOptionPane.showInputDialog(null, "Port: ", "SKIRMISH | JOIN", JOptionPane.QUESTION_MESSAGE);
			if(portStr == null){
				return 0;
			}else{
				try{
					port = Validate.port(portStr);
				}catch(InvalidPortException ipe){
					Utils.popupError(ipe.getMessage());
					port = 0;
				}
			}
		}
		return port;
	}

}
