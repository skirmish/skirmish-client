package exceptions;

public class SocketInitializationException extends RuntimeException {
	private static final long serialVersionUID = 1718659664879606430L;

	public SocketInitializationException() {
		super();
		
	}

	public SocketInitializationException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		
	}

	public SocketInitializationException(String message, Throwable cause) {
		super(message, cause);
		
	}

	public SocketInitializationException(String message) {
		super(message);
		
	}

	public SocketInitializationException(Throwable cause) {
		super(cause);
		
	}
	
}
