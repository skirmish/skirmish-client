package exceptions;

public class CounterExceededException extends RuntimeException {
	private static final long serialVersionUID = 2540411457861920315L;

	public CounterExceededException() {

	}

	public CounterExceededException(String message) {
		super(message);

	}

	public CounterExceededException(Throwable cause) {
		super(cause);

	}

	public CounterExceededException(String message, Throwable cause) {
		super(message, cause);

	}

	public CounterExceededException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);

	}

}
