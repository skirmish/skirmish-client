import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JPanel;


public class StatsPanel extends JPanel {
	private static final long serialVersionUID = -1603741918865049732L;
	private LoggerPanel statLogs;
	
	public StatsPanel(){
		statLogs = new LoggerPanel("Stats");
		statLogs.loggerBackground(Color.LIGHT_GRAY);
		setBackground(Color.GRAY);
		setPreferredSize(new Dimension(140,240));
		add(statLogs);
		show(new Stats());
	}
	
	public void show(Stats stats){
		statLogs.clear();
		statLogs.log("BASE HP: \n"+stats.getBaseHP());
		statLogs.log("WARRIORS: \n"+stats.getWarriors());
		statLogs.log("ARCHERS: \n"+stats.getArchers());
	}
}
