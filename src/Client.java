import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;

import exceptions.CounterExceededException;
import exceptions.InvalidServerAddressException;
import exceptions.PacketReceiveException;
import exceptions.PacketSendException;
import exceptions.SocketInitializationException;


public class Client {
	/*-----------SOCKET PROGRAMMING ATTRIBUTES----------*/
	private static final int MAXIMUM_BUFFER_SIZE = 1024;
	private static final int OWN_PORT = 9999;
	private InetAddress serverAddress;
	private int serverPort;
	private String username;
	private DatagramSocket socket;
	private volatile boolean alive;
	private Thread serverListener;
	private List<String> peers;
	/*---------------------------------------------------*/
	
	/*GUI ATTRIBUTE*/
	private ClientUI clientUI;
	private final Stats stats;
	private boolean connected;
	
	
	public Client(String username, String serverAddress, int serverPort){
		this.username = username;
		this.stats = new Stats();
		peers = new ArrayList<String>();
		
		connected = false;
		try {
			socket = new DatagramSocket(OWN_PORT);
			socket.setReuseAddress(true);
			setupListener();
			this.serverAddress = InetAddress.getByName(serverAddress);
			this.serverPort = serverPort; //server port
			setupClientUI();
			startServerListener();
			threeWayHandshake();
			clientUI.setStats(stats, this.username);
			display();
		} catch (UnknownHostException e1) {
			throw new InvalidServerAddressException("Unknown server address!");
		} catch (SocketException e) {
			throw new SocketInitializationException("Failed to initialize socket!");
		}
	}
	
	private void threeWayHandshake(){
		Utils.popupInformation("Connecting to server...");
		send("CONNECTION_REQUEST "+username);
		while(!connected){
			Utils.popupInformation("Still connecting...");
		}
		Utils.popupInformation("CONNECTED!");
		send("CONNECTED "+username);
	}
	
	private void setupClientUI() {
		clientUI = new ClientUI(this.username);
		setupButtons();
	}
	
	private void display(){
		clientUI.display();
	}

	private void setupButtons() {
		setupSendButton();
		setupSaveBaseButton();
		setupBuyButtons();
		setupAttackButton();
		clientUI.enableActionButtons(false);
		
	}
	
	private void setupAttackButton(){
		clientUI.setupAttackButton(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String defender = clientUI.getActivePanelUsername();
				if(!defender.equals(username)){
					String packetData = "ATTACK "+username+" "+defender;
					send(packetData);
				}
				
			}
		});
	}
	
	private void setupBuyButtons(){
		clientUI.setupBuyButtons(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				JButton source = (JButton)e.getSource();
				String unit = source.getText();
				try{
				switch(unit){
				case "ARCHER": 
					stats.addArchers(1);
					break;
				case "WARRIOR": 
					stats.addWarriors(1);
					break;
				}
				
				String trp = "TROOP "+username+ " "+stats.toString();
				System.out.println(trp);
				send(trp);
				clientUI.refreshStats(stats);
				}catch(CounterExceededException cee){
					Utils.popupError(cee.getMessage());
				}
				
			}
		});
	}
	
	private void setupSaveBaseButton(){
		clientUI.setupSaveBaseButton(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JButton source = (JButton)e.getSource();
				source.setEnabled(false);
				clientUI.disableEditBase();
				String baseState = clientUI.getHomeBaseState();
				if(!baseState.contains("C")){
					Utils.popupError("Please position your castle before saving!");
					return;
				}
				String packetData = "BASE_STATE "+username+" "+baseState;
				send(packetData);
			}
		});
	}

	private void setupSendButton() {
		clientUI.setupSendButton(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				String chatMessage = clientUI.getChatMessage();
				if(chatMessage.length() == 0){
					return;
				}
				String packetData = "CHAT " + username + " \""+chatMessage+"\"";
				send(packetData);
				clientUI.clearChatMessage();
			}
		});
	}

	private void setupListener() {
		serverListener = new Thread(new Runnable(){
			@Override
			public void run() {
				alive = true;
				while(alive){ //listen to server
					//one byte per character, so whats the maximum size?
					byte[] buff = new byte[MAXIMUM_BUFFER_SIZE];
					DatagramPacket packet = new DatagramPacket(buff, buff.length);
					
					try {
						socket.receive(packet);
						
						String message = new String(buff).trim();
						
						if(message.startsWith("CHAT")){
							String packetdata = message;
							packetdata = packetdata.replaceFirst("^CHAT", "").trim();
							String[] data = packetdata.split("\\s", 2);
							data[1] = data[1].replaceAll("\\\"", "");
							//data 0 is username, data 1 is message.
							clientUI.appendChatMessage(data[0].equals(username)?"YOU":data[0],data[1]);
						}else if(message.startsWith("CONNECTION_GRANTED")){
							connected = true;
							send("CONNECTED "+username);
						}else if(message.startsWith("CONNECTED")){
							String username_new = message.replaceFirst("^CONNECTED", "").trim();
							if(!peers.contains(username_new)){
								clientUI.addPlayerToWaitingArea(username_new);
								clientUI.createBaseFor(username_new);
								peers.add(username_new);
							}
							
						}else if(message.startsWith("BATTLE_BEGIN")){
							clientUI.show(username);
							clientUI.enableActionButtons(true);
							clientUI.enableJumpButtons(true);
						}else if(message.startsWith("BASE_STATE")){
							String[] tokens = message.split("\\s");
							String username = tokens[1];
							String baseState = tokens[2];
							clientUI.updateBaseState(username, baseState);
						}else if(message.equals("DISABLE")){
							clientUI.enableControls(false);
						}else if(message.equals("ENABLE")){
							clientUI.enableControls(true);
						}else if(message.startsWith("PLEASE")){
							String msg = message.split("\\s")[1];
							Utils.popupInformation(msg.replaceAll("_", " "));
						}else if(message.equals("RESET STATS")){
							stats.reset();
							clientUI.refreshStats(stats);
						}else if(message.startsWith("VICTORY")){
							String victor = message.split("\\s")[1];
							Utils.popupInformation(victor+" is VICTORIOUS!\nGAME OVER!");
							Utils.popupInformation("GAME WILL NOW EXIT!");
							System.exit(1);
						}
						else{
							System.out.println("Unhandled packet received:" +message);
						}
						
						
						
					} catch (IOException e) {
						throw new PacketReceiveException("Failed to receive packet!");
					}
				}
			}
		});
	}
	
	private void startServerListener(){
		serverListener.start();
	}
	
	public void kill(){
		serverListener.interrupt();
		alive = false;
	}
	
	public void send(String message){
		byte[] messageBuffer = message.getBytes();
		try {
			DatagramPacket packet = new DatagramPacket(messageBuffer, 
														messageBuffer.length, 
														serverAddress, serverPort);
			socket.send(packet);
		} catch (IOException e){
			throw new PacketSendException("Packet "+message+" not sent!");
		}
	}
	
	public String getUsername(){
		return username;
	}
	
	@Override
	public String toString(){
		StringBuilder builder = new StringBuilder();
		
		return builder.toString();
	}
}
